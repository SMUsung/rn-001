import React, {Component} from 'react'
import {View,Text,StyleSheet} from "react-native"

class App extends Component{
  render(){
    return(
      <View style = {styles.mainView}>
        <View style = {styles.subView1}>
          <Text style = {{color: "red", fontSize:20}}> SubView 1 </Text>
        </View>
        <View style = {styles.subView2}></View>
        <Text>
          Hello World !!!
        </Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  mainView:{
    backgroundColor:'green',
    marginLeft:50,
    flex:1,
  },
  subView1:{
    backgroundColor:'blue',
    flex:1,
  },
  subView2:{
    backgroundColor:'red',
    flex:1,
  }
})


export default App;